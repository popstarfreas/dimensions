interface Packet {
    packetType: number;
    data: string;
}

export default Packet;