interface RoutingServer {
    name: string;
    serverIP: string;
    serverPort: number;
}

export default RoutingServer;