interface ServerDetails {
    clientCount: number;
    disabled: boolean;
    failedConnAttempts: number;
}

export default ServerDetails;