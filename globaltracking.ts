interface GlobalTracking {
    names: { [name: string]: boolean }
}

export default GlobalTracking;