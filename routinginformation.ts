interface RoutingInformation {
    type: number;
    info: string;
}

export default RoutingInformation;